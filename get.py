from flask import Flask , jsonify,render_template
from flask_mongoengine import MongoEngine 
import pandas as pd 
from pymongo import MongoClient
import json
from bson import json_util


app=Flask(__name__)

c=MongoClient()

@app.route("/getdata")
def getdata():
	db=c.test
	col=db.sports
	hold=[]
	for i in range(0,10):
		hold.append(col.find()[i])
 	data=json.dumps(hold,default=json_util.default)
 	return jsonify(data=json.loads(data))
	

@app.route('/')
def index():
	return render_template('main.html')

if __name__ == '__main__':
    app.run(debug=True)